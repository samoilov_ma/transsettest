package com.example.samoilov_ma.testapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samoilov_ma on 05.08.2016.
 * Class for working with `test` table
 */
public class TestTable extends DBWorker {

    /**************** Table columns ****************/
    /**
     * id - INTEGER PRIMARY KEY AUTOINCREMENT
     */
    public long id;

    /**
     * name - VARCHAR(255)
     */
    public String name;

    /**
     * value - DOUBLE
     */
    public double value;

    /**
     * datetime - DATETIME DEFAULT CURRENT_TIMESTAMP
     */
    public String datetime;
    /************************************************/

    /**
     * Tag for logging
     */
    private final static String TAG = "TestTable";

    /**
     * PK like that can not exist, useful for falsity detection
     */
    private final static int FAKE_PK = -1;

    /**
     * It is necessary for open or create the database
     */
    private Context context;

    /**
     * Public constructor for TestTable
     * @param context - It is necessary for open or create the database
     */
    public TestTable(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * Checks whether the table exists
     * @return true if the table exists, false if doesn't
     */
    public boolean whetherTableExists() {
        try {
            dataBase = mDBHelper.getReadableDatabase();
            Cursor cursor = dataBase.rawQuery(
                    "SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?",
                    new String[]{"table", DBWorker.TestTableEntry.TABLE_NAME});
            if (!cursor.moveToFirst()) {
                return false;
            }
            int count = cursor.getInt(0);
            cursor.close();
            Log.i(TAG, "whetherTableExists() have been worked successfully");
            return count > 0;
        } catch (Exception e) {
            Log.e(TAG, "Exception in whetherTableExists() is " + e.toString());
            return false;
        } finally {
            if (dataBase != null) {
                dataBase.close();
            }
        }
    }

    /**
     * Inserts data to the table
     * @param data2insert - list of names to insert
     */
    public void insertDataList(List<String> data2insert) {
        try {
            dataBase = mDBHelper.getWritableDatabase();
            dataBase.beginTransaction();
            ContentValues cv = new ContentValues();

            for (String name : data2insert) {
                cv.put(DBWorker.TestTableEntry.COLUMN_NAME, name);
                dataBase.insertOrThrow(DBWorker.TestTableEntry.TABLE_NAME, null, cv);
            }

            dataBase.setTransactionSuccessful();
            Log.i(TAG, "insertDataList() have been worked successfully");
        } catch (Exception e) {
            Log.e(TAG, "Exception in insertDataList() is " + e.toString());
        } finally {
            if (dataBase != null) {
                dataBase.endTransaction();
                dataBase.close();
            }
        }
    }

    /**
     * @return all records from the table
     */
    public ArrayList<TestTable> getData() {
        return getData(FAKE_PK);
    }

    /**
     * @param pk - id of required record
     * @return a record with specified PK
     */
    public ArrayList<TestTable> getData(int pk) {

        ArrayList<TestTable> dataList = new ArrayList<>();
        Cursor cursor = null;
        boolean isFakePk = pk == FAKE_PK;

        try {
            dataBase = mDBHelper.getReadableDatabase();
            cursor = dataBase.query(
                    DBWorker.TestTableEntry.TABLE_NAME,
                    new String[]{
                            DBWorker.TestTableEntry.COLUMN_ID,
                            DBWorker.TestTableEntry.COLUMN_NAME,
                            DBWorker.TestTableEntry.COLUMN_VALUE,
                            DBWorker.TestTableEntry.COLUMN_DATE
                    },
                    isFakePk ? null : DBWorker.TestTableEntry.COLUMN_ID + " = ?",
                    isFakePk ? null : new String[]{Integer.toString(pk)},
                    null,
                    null,
                    isFakePk ? DBWorker.TestTableEntry.COLUMN_NAME : null
            );

            cursor.moveToFirst();
            dataList = populateArrayListByData(cursor);
            Log.i(TAG, "getRecordByPkFromTestTable() have been worked successfully");

        } catch (Exception e) {
            Log.e(TAG, "Exception in getRecordByPkFromTestTable() is " + e.toString());
        } finally {
            if (dataBase != null) {
                dataBase.close();
            }
            if (cursor != null) {
                cursor.close();
            }
        }
        return dataList;
    }

    /**
     * Updates a record with specified PK
     * @param pk - id of a record to update
     * @param newValue - new value for `value` column
     */
    public void updateValueByPk(int pk, double newValue) {
        try {
            dataBase = mDBHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(DBWorker.TestTableEntry.COLUMN_VALUE, newValue);

            dataBase.update(
                    DBWorker.TestTableEntry.TABLE_NAME,
                    cv,
                    DBWorker.TestTableEntry.COLUMN_ID + " = ? ",
                    new String[]{Integer.toString(pk)}
            );

            Log.i(TAG, "updateValueByPk() have been worked successfully");
        } catch (Exception e) {
            Log.e(TAG, "Exception in updateValueByPk() is " + e.toString());
        } finally {
            if (dataBase != null) {
                dataBase.close();
            }
        }
    }

    /**
     * Deletes all data at the table
     */
    public void deleteData() {
        deleteData(FAKE_PK);
    }

    /**
     * Deletes a record with specified PK
     * @param pk - id of a record to remove
     */
    public void deleteData(int pk) {
        boolean isFakePk = pk == FAKE_PK;
        try {
            dataBase = mDBHelper.getWritableDatabase();

            dataBase.delete(
                    DBWorker.TestTableEntry.TABLE_NAME,
                    isFakePk ? null : DBWorker.TestTableEntry.COLUMN_ID + " = ? ",
                    isFakePk ? null : new String[]{Integer.toString(pk)}
            );

            Log.i(TAG, "deleteRecordByPkAtTestTable() have been worked successfully");
        } catch (Exception e) {
            Log.e(TAG, "Exception in deleteRecordByPkAtTestTable() is " + e.toString());
        } finally {
            if (dataBase != null) {
                dataBase.close();
            }
        }
    }

    /**
     * Inserts specified amount of rows to the table inside transaction
     * @param n - amount of rows to insert
     */
    public void insertNRecordsInsideTransaction(int n) {
        try {
            dataBase = mDBHelper.getWritableDatabase();
            dataBase.beginTransaction();
            insertNRecords(n);
            dataBase.setTransactionSuccessful();
            Log.i(TAG, "insertNRecordsInsideTransaction() have been worked successfully");
        } catch (Exception e) {
            Log.e(TAG, "Exception in insertNRecordsInsideTransaction() is " + e.toString());
        } finally {
            if (dataBase != null) {
                dataBase.endTransaction();
                dataBase.close();
            }
        }
    }

    /**
     * Inserts specified amount of rows to the table outside transaction
     * @param n - amount of rows to insert
     */
    public void insertNRecordsOutsideTransaction(int n) {
        try {
            dataBase = mDBHelper.getWritableDatabase();
            insertNRecords(n);
            Log.i(TAG, "insertNRecordsOutsideTransaction() have been worked successfully");
        } catch (Exception e) {
            Log.e(TAG, "Exception in insertNRecordsOutsideTransaction() is " + e.toString());
        } finally {
            if (dataBase != null) {
                dataBase.close();
            }
        }
    }

    /**
     * Inserts specified amount of rows to the table
     * @param n - amount of rows to insert
     */
    private void insertNRecords(int n) {
        try {
            ContentValues cv = new ContentValues();
            for (int i = 0; i < n; i++) {
                cv.put(DBWorker.TestTableEntry.COLUMN_NAME, "name" + i);
                cv.put(DBWorker.TestTableEntry.COLUMN_VALUE, i + 0.1 * i);
                dataBase.insertOrThrow(DBWorker.TestTableEntry.TABLE_NAME, null, cv);
            }
            Log.i(TAG, "insertNRecords() have been worked successfully");
        } catch (Exception e) {
            Log.e(TAG, "Exception in insertNRecords() is " + e.toString());
        }
    }

    /**
     * @param cursor - data from DB
     * @return ArrayList of TestTables with data from cursor
     */
    private ArrayList<TestTable> populateArrayListByData(Cursor cursor) {
        ArrayList<TestTable> dataList = new ArrayList<>();

        try {
            for (int i = 0; i < cursor.getCount(); i++) {
                TestTable row = new TestTable(context);
                row.id = cursor.getLong(cursor.getColumnIndexOrThrow(DBWorker.TestTableEntry.COLUMN_ID));
                row.name = cursor.getString(cursor.getColumnIndexOrThrow(DBWorker.TestTableEntry.COLUMN_NAME));
                row.datetime = cursor.getString(cursor.getColumnIndexOrThrow(DBWorker.TestTableEntry.COLUMN_DATE));
                row.value = cursor.getDouble(cursor.getColumnIndexOrThrow(DBWorker.TestTableEntry.COLUMN_VALUE));
                dataList.add(row);

                cursor.moveToNext();
            }
            Log.i(TAG, "populateArrayListByData() have been worked successfully");
        } catch (Exception e) {
            Log.e(TAG, "Exception in populateArrayListByData() is " + e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return dataList;
    }
}
