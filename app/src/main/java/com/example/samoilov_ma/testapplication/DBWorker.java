package com.example.samoilov_ma.testapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by samoilov_ma on 03.08.2016.
 * Base class for working with the database
 */
public class DBWorker {

    protected SQLiteDatabase dataBase;
    protected DBHelper mDBHelper;

    /**
     * Public constructor for DBWorker
     * @param context - It is necessary for open or create the database
     */
    public DBWorker(Context context) {
        mDBHelper = new DBHelper(context);
        dataBase = null;
    }

    /**
     * Abstract class with table and column names for `test` table
     */
    abstract class TestTableEntry {
        public static final String TABLE_NAME = "test";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_VALUE = "value";
        public static final String COLUMN_DATE = "datetime";
    }

    /**
     * Class to manage database creation and version management
     */
    public class DBHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 4;
        public static final String DATABASE_NAME = "test.db";

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createTestTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            dropTestTable(db);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }

    /**
     * Creates `test` table
     */
    public void createTestTable() {
        createTestTable(mDBHelper.getWritableDatabase());
    }

    /**
     * Creates `test` table
     * @param db - database object
     */
    private void createTestTable(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TestTableEntry.TABLE_NAME + " (" +
                TestTableEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TestTableEntry.COLUMN_NAME + " VARCHAR(255), " +
                TestTableEntry.COLUMN_VALUE + " DOUBLE, " +
                TestTableEntry.COLUMN_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")");
    }

    /**
     * Drops `test` table
     */
    public void dropTestTable() {
        dropTestTable(mDBHelper.getWritableDatabase());
    }

    /**
     * Drops `test` table
     * @param db - database object
     */
    private void dropTestTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TestTableEntry.TABLE_NAME);
    }
}
